﻿<?php
  session_start();
  if(!isset($_SESSION["tabela"])){ //Testa se existe ou não a sessão
    $_SESSION["tabela"] = array(array("nd", "nd", "nd"), array("nd", "nd", "nd"), array("nd", "nd", "nd")); //Atribuir os valores "nd" à sessão tabela, onde "nd" é o nome da imagem em forma de matriz 
    $_SESSION["liberado"] = "off"; //Atribui o valor "off" à sessão liberado, onde "off" quer dizer que não se pode realizar a jogada 
    $_SESSION["jogador"] = 1; //Atribui o valor "1" à sessão jogador, onde "1" quer dizer que o jogador 1 que fará a jogada
    $_SESSION["jogada"] = "x"; //Atribui o valor "x" à sessão jogada, onde "x" quer dizer que a imagem X aparecerá
  }
?>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	</head>
<body  text="white" alink="White" vlink="NavajoWhite" link="NavajoWhite" background="img/quimica.jpg">
 <table width="100%">
  <tr>
  <td colspan="2" width="100%" align="center" valign="top"> <font size="6" face="arial black"> Velha Molar </font></td>
  <td rowspan="2" width="100%" height="100%"><img src="img/tabelacomp.png" width="600px" height="90%"></td>
  </tr>
  <tr>
  <td colspan="2" width = "100%"><table border="0" width="100%" height="100%" bgcolor="black" color='red'>
  <tr>
  <td><a href="jogando.php?linha=0&coluna=0"><img src="<?php echo 'img/' . $_SESSION['tabela'][0][0] . '.PNG'?>" width="100%" height="100%"></a></td> 
  <!-- Onde "jogando.php?linha=0&coluna=0", mostra para a matriz a posição marcada no link e $_SESSION['tabela'][0][0], pega valor nessa posição na matriz. Obs.: o mesmo vale para os itens abaixo -->
  <td><a href="jogando.php?linha=0&coluna=1"><img src="<?php echo 'img/' . $_SESSION['tabela'][0][1] . '.PNG'?>" width="100%" height="100%"></a></td>
  <td><a href="jogando.php?linha=0&coluna=2"><img src="<?php echo 'img/' . $_SESSION['tabela'][0][2] . '.PNG'?>" width="100%" height="100%"></a></td>
  </tr>
  <tr>
  <td><a href="jogando.php?linha=1&coluna=0"><img src="<?php echo 'img/' . $_SESSION['tabela'][1][0] . '.PNG'?>" width="100%" height="100%"></a></td>
  <td><a href="jogando.php?linha=1&coluna=1"><img src="<?php echo 'img/' . $_SESSION['tabela'][1][1] . '.PNG'?>" width="100%" height="100%"></a></td>
  <td><a href="jogando.php?linha=1&coluna=2"><img src="<?php echo 'img/' . $_SESSION['tabela'][1][2] . '.PNG'?>" width="100%" height="100%"></a></td>
  </tr>
  <tr>
  <td><a href="jogando.php?linha=2&coluna=0"><img src="<?php echo 'img/' . $_SESSION['tabela'][2][0] . '.PNG'?>" width="100%" height="100%"></a></td>
  <td><a href="jogando.php?linha=2&coluna=1"><img src="<?php echo 'img/' . $_SESSION['tabela'][2][1] . '.PNG'?>" width="100%" height="100%"></a></td>
  <td><a href="jogando.php?linha=2&coluna=2"><img src="<?php echo 'img/' . $_SESSION['tabela'][2][2] . '.PNG'?>" width="100%" height="100%"></a></td>
  </tr>
  </table>
  </td>
  </tr>
  <tr>
  <td valign="top"><img src="img/circ.PNG" width="10%" height="15%"><font size="3" face="arial black"> Jogador 1 <!-- Descreve a imagem que refere-se ao jogador 1 (Círculo) -->
  <img src="img/x.PNG" width="10%" height="15%"> Jogador 2</font></td> <!-- Descreve a imagem que refere-se ao jogador 2 (X) -->
  <td valign="top"> <a href="pergunta.php" target="cont" style='text-decoration:none'><font size="4" face="arial black"> Jogar </font> <!-- Função "Jogar", onde ao clicar, as perguntas e alternativas 
  serão selecionadas no banco de dados, sorteadas de maneira aleatória e impressas na tela -->
  </td>
  <td rowspan="2"><iframe align="rigth" name="cont" frameborder="0" width="100%" height="210px"></iframe></td>
  </tr>
  <tr><td align="center" valign="top" colspan="2" colspan='2'><a href="restart.php" style='text-decoration:none'> Reiniciar </td> <!-- Função "Reiniciar", onde ao clicar o jogo será reiniciado, desde que já 
  tenha sido selecionada a opção Jogar, cancelando todas as ações em andamento --> 
  </tr> 
 </table>
</body>
</html>
