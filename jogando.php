﻿<?php
  session_start();
  $velha = ""; //Declara $velha como vazio ou nada
   if($_SESSION["liberado"] == "on"){ //Se a sessão liberado for igual a "on", poderá fazer a jogada
  
     $linha = $_GET["linha"]; //Pega o valor de linha enviada no link
     $coluna = $_GET["coluna"]; //Pega o valor de coluna enviada no link
     if($_SESSION["jogador"] == 1){ //Se o jogador for igual a 1, então a sessão jogada é igual a "x" (nome da imagem) 
     $_SESSION["jogada"] = "x";
     } else if($_SESSION["jogador"] == 2){ //Senão, se o jogador for igual a 2, então a sessão jogada é igual a "cir" (nome da imagem)
     $_SESSION["jogada"] = "circ";
     }
     if($_SESSION["tabela"][$linha][$coluna] == 'nd'){ //Se a sessão tabela for igual a nd, então
     $_SESSION["tabela"][$linha][$coluna] = $_SESSION["jogada"]; //Atribui à sessão tabela os valores da sessão jogada na posição dos valores de linha e coluna
     $_SESSION["liberado"] = "off"; //A sessão liberado volta a ser "off"
    } else {
     $_SESSION["liberado"] = "on";	//Senão, continua "on", para efetuar a jogada
    } 
  }
if((($_SESSION["tabela"][0][0] != 'nd') && ($_SESSION["tabela"][0][1] != 'nd') &&
($_SESSION["tabela"][0][2] != 'nd') && ($_SESSION["tabela"][0][0] == $_SESSION["tabela"][0][1]) && 
($_SESSION["tabela"][0][0] == $_SESSION["tabela"][0][2])) 
|| (($_SESSION["tabela"][1][0] != 'nd' ) && ($_SESSION["tabela"][1][1] != 'nd') && 
($_SESSION["tabela"][1][2] != 'nd') && ($_SESSION["tabela"][1][0] == $_SESSION["tabela"][1][1]) && 
($_SESSION["tabela"][1][0] == $_SESSION["tabela"][1][2])) 
|| (($_SESSION["tabela"][2][0] != 'nd') && ($_SESSION["tabela"][2][1] != 'nd') &&
($_SESSION["tabela"][2][2] != 'nd') && ($_SESSION["tabela"][2][0] == $_SESSION["tabela"][2][1]) && 
($_SESSION["tabela"][2][0] == $_SESSION["tabela"][2][2])) 
|| (($_SESSION["tabela"][0][0] != 'nd') && ($_SESSION["tabela"][1][0] != 'nd') && 
($_SESSION["tabela"][2][0] != 'nd') && ($_SESSION["tabela"][0][0] == $_SESSION["tabela"][1][0]) && 
($_SESSION["tabela"][0][0] == $_SESSION["tabela"][2][0]))
|| (($_SESSION["tabela"][0][1] != 'nd') && ($_SESSION["tabela"][1][1] != 'nd') &&
($_SESSION["tabela"][2][1] != 'nd') && ($_SESSION["tabela"][0][1] == $_SESSION["tabela"][1][1]) && 
($_SESSION["tabela"][0][1] == $_SESSION["tabela"][2][1])) 
|| (($_SESSION["tabela"][0][2] != 'nd') && ($_SESSION["tabela"][1][2] != 'nd') && 
($_SESSION["tabela"][2][2] != 'nd') && ($_SESSION["tabela"][0][2] == $_SESSION["tabela"][1][2]) && 
($_SESSION["tabela"][0][2] == $_SESSION["tabela"][2][2])) 
|| (($_SESSION["tabela"][0][0] != 'nd') && ($_SESSION["tabela"][1][1] != 'nd') &&
($_SESSION["tabela"][2][2] != 'nd') && ($_SESSION["tabela"][0][0] == $_SESSION["tabela"][1][1]) && 
($_SESSION["tabela"][0][0] == $_SESSION["tabela"][2][2])) 
|| (($_SESSION["tabela"][0][2] != 'nd') && ($_SESSION["tabela"][1][1] != 'nd') && 
($_SESSION["tabela"][2][0] != 'nd') && ($_SESSION["tabela"][0][2] == $_SESSION["tabela"][1][1]) && 
($_SESSION["tabela"][0][2] == $_SESSION["tabela"][2][0]))){ //Caso depois de todas as comparações, em diagonal, horizontal ou vertical, consiga uma sequência de três símbolos iguais, há um vencedor
header("Location: vencedor.php "); //Vai para a página "vencedor.php"

} else if(($_SESSION["tabela"][0][0] != 'nd') && ($_SESSION["tabela"][0][1] != 'nd') &&
($_SESSION["tabela"][0][2] != 'nd') && ($_SESSION["tabela"][1][0] != 'nd' ) && 
($_SESSION["tabela"][1][1] != 'nd') && ( $_SESSION["tabela"][1][2] != 'nd') &&
($_SESSION["tabela"][2][0] != 'nd') && ($_SESSION["tabela"][2][1] != 'nd') &&
($_SESSION["tabela"][2][2] != 'nd')){ //Caso depois de todas as comparações, ninguém consiga a sequência de símbolos necessária, diz-se que o jogo deu velha
header("Location: velha.php"); //Redireciona-se para a página "velha.php"

} else {
header("Location: index.php"); //Se não, continua em "index.php"
}
?>
