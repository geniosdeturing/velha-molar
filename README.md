É inegável a necessidade do ensino de química na sala de aula. 
Porém, esse, vem sendo desvinculado da realidade do ser humano, fazendo com que essa disciplina não seja atrativa aos discentes. 
Uma possível alternativa trata-se dos jogos didáticos digitais, nesse artigo, o software educacional “Velha Molar”. 
A ideia é aliar tecnologia e educação, deixando o tradicionalismo em prol de uma proposta contemporânea para o aprendizado de conceitos químicos.

Para utilizar o projeto:
1) Necessário possuir um ambiente PHP + MySQL;
2) Importar o arquivo de banco de dados "Script-VelhaMolar.sql" presente no diretório Script;
3) Alterar as configurações de conexão ao banco de dados no arquivo "conexao.php" presente no diretório raiz;
4) E divertir bastante!

Contato: Adriano Honorato Braga - contato@adrianobraga.com.br
         Thalia de Santos Santana - thaliassantana15@gmail.com